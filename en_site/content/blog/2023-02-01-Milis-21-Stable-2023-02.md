<!-- title: Milis Linux 2.1 Stable February Release -->

<!--2023/02/02-->
After our seasonal works, our new stable image is ready as dated 2023-02-02.
In this release, mostly works are related on the desktop and its applications with the core system remaining the same.
Also our desktop configuration structure has been abstracted and a new format of desktop configuration has been applied which supports different window managers.

In addition, the works done for this release:

- A new desktop configuration that supports different window managers.
- With this configuration, a new script has been written called as dinit that controls the desktop operations.
- Two window managers are used Wayfire(Tiling) and Labwc(Stacking).
- A new login manager interface has been developed which uses Greetd backend application instead of Ly login manager.
- With the new login manager, users can choose  different window manager and language.
- Application Assignment and user-defined Keyboard settings module has been added to the Setting Center(Ayguci) application.
- Added fast screen recorder, screenshot and weather plugins to the desktop panel.
- Added features to use as an alternative functional file manager to the Fterm application.
- MPDF application has been developed and added to the application menu for reading PDF documents.
- System-config-printer is the new default printer management interface.
- Swappy application is also in the panel to provide functional support for our MEG snipping application.
- Display management configuration has been updated and added to the Settings Center to run with different window managers.
- Instead of the wayland-logout application, a simple script has been written.
- Quick install scripts for Go, Rust and Zig programming languages.
- The ability to open multiple tabs has been patched to Terminal.
- Updated to version 1.38, adding terminal support to the Geany text editor.
- Firefox browser updated to version 105.0.1.
- Updated QT graphics packages to version 5.15.6.
- In addition to the Celluloid video player, the VLC application has also been added to the repository.
- The htop process manager has been updated to version 3.2.1 so that it can be accessed from the console instead of the menu.
- Desktop applications and menus have been adjusted to support Turkish and English.
- The 5.16.14 version is still used as the Linux kernel, and it is planned to switch to 6.x in future versions.
- The list of default applications used on the desktop can be accessed [here](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/masaustu.md).

Download link:

[Milis Linux 2.1 2023-02-02 Desktop](https://mls.akdeniz.edu.tr/iso/2.1/milis-2.1-desktop-2023.02.02.iso)

[sha26sum](https://mls.akdeniz.edu.tr/iso/2.1/milis-2.1-desktop-2023.02.02.iso.sha256sum.txt)


<!-- title: Download -->
## Milis Linux 2.3

- [Milis 2.3 Desktop (February 2025)](https://mls.akdeniz.edu.tr/iso/2.3/beta/milis-2.3-desktop-2025.02.03.iso) -
[shasum](https://mls.akdeniz.edu.tr/iso/2.3/beta/milis-2.3-desktop-2025.02.03.iso.sha256sum.txt)

- [Milis 2.3 Minimal (February 2025)](https://mls.akdeniz.edu.tr/iso/2.3/milis-2.3-minimal-2025.02.03.iso) -
[shasum](https://mls.akdeniz.edu.tr/iso/2.3/milis-2.3-minimal-2025.02.03.iso.sha256sum.txt)


## Milis Linux 2.1

- [Milis 2.1 Desktop Stable (February 2023)](https://mls.akdeniz.edu.tr/iso/2.1/milis-2.1-desktop-2023.02.02.iso) -
[shasum](https://mls.akdeniz.edu.tr/iso/2.1/milis-2.1-desktop-2023.02.02.iso.sha256sum.txt)

- [Milis 2.1 Minimal Stable (March 2022)](https://mls.akdeniz.edu.tr/iso/2.1/milis-2.1-minimal-2022.03.27.iso) -
[shasum](https://mls.akdeniz.edu.tr/iso/2.1/milis-2.1-minimal-2022.03.27.iso.sha256sum.txt)

### Login

<pre>username : mls / password : mls </pre>

After network connection please pull updates:

<pre>mps gun</pre>

After then for installation:

<pre>milis-kur</pre>

--------------------------------------

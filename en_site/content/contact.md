<!-- title: Contact -->
### Contact
- [E-mail](mailto:milisarge@gmail.com)
- [Forum](http://mls.akdeniz.edu.tr/forum/forum)
- [GIT](https://mls.akdeniz.edu.tr/git/milislinux/milis21)
- [Team Chat (Mattermost)](https://mls.akdeniz.edu.tr/mm)
- [Social Media (Mastodon)](https://mastodon.social/@milislinux)
- [Chat (IRC)](https://kiwiirc.com/nextclient/irc.libera.chat/#milislinux)

**Note**: Friends who want to contribute continuously, can contact milisarge at gmail.com for GIT and Mattermost registration.

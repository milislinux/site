<img src="/image/logo.png" alt="drawing" width="90"/> 

## Milis Linux - Milli İşletim Sistemi

Milis Linux (<b>Mil</b>li <b>İ</b>şletim <b>S</b>istemi) ülkemizdeki yerli işletim sistemi çalışmalarının yetersiz ve sekteye uğramış olması göz önünde bulundurularak 2016 yılında gönüllü olarak başlatılmış, 2019-2020 yıllarında Akdeniz Üniversitesi BAP Projesi olarak desteklenmiş ve halen geliştirilmekte olan Linux tabanlı bir işletim sistemi projesidir.

Milis Linux özgünlüğü yakalamak adına kendi "Sıfırdan Linux" yapım tekniğini kullanmaktadır. Bu doğrultuda kendi paket yönetim sistemine (Milis Paket Sistemi) ve özgün uygulamalarına sahiptir.

Milis Linux Projesi, açık kaynak ve milli yazılım geliştirme prensiplerine dayanmakta olup ülkemizin bilişimdeki katma değerinin artırılmasını ideal edinmiştir.

### Milis Linux 2.3 (Mahtumkulu) Ana Özellikler:

- x86-64 bit sistem (Multilib - RISC-V planlanmakta)
- MPS - özgün paket yöneticisi (Lua, Go)
- Wayland destekli masaüstü ortamı
- Yeni bileşik paketleme yaklaşımı
- IPFS destekli dağıtık paket deposu
- Ayguci - modüler ayar ve bilgi sistemi
- Militer başlatıcı sistemi (Busybox init + Lua görev betikleri)
- Kolay paketleme sistemi (INI biçiminde talimat dosyaları)
- Farklı dillerde konsol uygulamaları barındırma (Rust, Go, Zig)
- Temiz ve kararlı sistem
- Güncel ve kararlı paket deposu
- Gönüllü ve üretken katkıcı desteği

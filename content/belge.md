<!-- title: Belge -->
### Belgeler

- [Katkı ve Destek Notları](https://gitlab.com/milislinux/milis23/-/blob/main/belge/destek.md)
- [Kurulum](/doc/kurulum)
- [Milis Paket Sistemi](https://gitlab.com/milislinux/milis23/-/blob/main/belge/mps.md)
- [Servis Yönetimi](https://gitlab.com/milislinux/milis23/-/blob/main/belge/servis.md)
- [Masaüstü Ortamı ve Varsayılan Uygulamalar](https://gitlab.com/milislinux/milis23/-/blob/main/belge/masaustu.md)
- [Ayguci Modüler Ayar Sistemi](https://gitlab.com/milislinux/milis23/-/blob/main/belge/ayguci.md)
- [Saat/Tarih Ayarı](https://gitlab.com/milislinux/milis23/-/blob/main/belge/saat_tarih.md)
- [Masaüstü Kısayolları](https://gitlab.com/milislinux/milis23/-/blob/main/belge/kisayollar.md)
- [Sistem Test Maddeleri](https://gitlab.com/milislinux/milis23/-/blob/main/belge/sistem_test.md)
- [Paketleme](https://gitlab.com/milislinux/milis23/-/blob/main/belge/paketleme.md)
- [Canlı/Kurulum İmaj Üretimi](https://gitlab.com/milislinux/milis23/-/blob/main/belge/imaj-uretimi.md)
- [Derleme Ortamı Üretimi](https://gitlab.com/milislinux/milis23/-/blob/main/belge/derleme-ortami-uretme.md)
- [Milis 2.3 Sıfırdan Yapım Rehberi](https://gitlab.com/milislinux/milis23/-/blob/main/belge/mlfs.md)
- [Diğer Belgeler](https://gitlab.com/milislinux/milis23/-/blob/main/belge)


### Akademik Çalışmalar

- [Linux Tabanlı Hafif ve Açık Kaynak Kodlu Büyük Veri Dağıtımı Gerçeklemesi](https://tez.yok.gov.tr/UlusalTezMerkezi/TezGoster?key=aEzj_IdWAsjiSAfK3qwrBtYatSKohvG_E7ljeRqDQ5VygI1_MRFprtVbLB0wlGpY)
- [Building An Open Source Linux Computing System On RISC-V](https://ieeexplore.ieee.org/document/8965559)
- [Blockchain Based Distributed Package Management Architecture](https://ieeexplore.ieee.org/document/9219374)
- [Building an Open Source Big Data Platform Based on Milis Linux](https://link.springer.com/chapter/10.1007/978-3-030-79357-9_5)


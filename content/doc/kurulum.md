<!-- title: Kurulum -->

### Kurulum

Milis Linux'ta çalışan iso dosyasından, sistemi direkt olarak hedef disk bölümüne 
kurabilirsiniz. Bu aşamada iso dosyası üzerinde yaptığınız ayarlar da olduğu gibi
yeni sisteme aktarılacaktır.
Kurulum öncesi

#### İmza Doğrulaması
Herhangi bir dosya bozukluğuna ve indirme hatasına karşın, yüklemeye başlamadan önce iso
dosyasının imzasını doğrulamanız önerilir.
Bunu yapmak için indirdiğiniz sha256sum dosyasını iso dosyanız ile aynı klasöre atın ve alttaki 
komutu indiriğiniz imza dosyası ile çalıştırın:

`
	sha256sum -c milis-2.1-desktop-B08.01.iso.sha256sum
`

#### Canlı İmajın Hazırlanması
Kurulum için imaj dosyası, en az 2GB boyutlu bir USB belleğe yazılmalıdır.
Yazma öncesi **`lsblk`** veya ***`blkid`*** komutları yardımıyla yazılacak diskin yolu tespit edilebilir.
Linux sistemlerde imaj dosyası aşağıdaki şekilde yazılabilir:

```
dd if=milis-2.1-dekstop-aa.gg.iso of=/dev/sdx bs=4M

sync
```

Linux harici sistemler için ilgili işletim sistemi için USB bellek yazma uygulamaları kullanılabilir.

#### Sanal Makine Ayarları
Canlı imaj, sanal makine ile de başlatılıp kullanılabilir. 
Fakat sanal makine başlatılırken bazı parametrelerin verilmesi gerekmektedir.
    
    - Qemu: "-vga qxl" parametresi eklenmeli.
    - Virtualbox: Graphisc Controller olarak VMSVGA ve 3D accelaration enable seçili olmalıdır.

####  Sistemin Canlı İmaj ile Başlatılması
İndirdiğiniz iso dosyasını bir USB belleğe veya CD sürücüye yazdırın ve bilgisayarınızı bu
sürücü ile başlatın. Canlı imaj fiziksel veya sanal ortamda başladıktan sonra Milis Linux'u üç yolla bilgisayarınıza kurabilirsiniz:

### A- Grafiksel Arayüzlü Kurulum

Masaüstündeki kurulum uygulaması ile grafiksel arayüz kullanılarak ta kurulabilir.
Uygulamadaki adımlarda gerekli bilgiler mevcuttur.


### B- Konsol Arayüzlü Kurulum

Bir uçbirim(terminal) uygulaması açıp **milis-kur** komutu vererek kurulum uygulamasını açılır.
Milis-kur uygulaması konsol tabanlı arayüze sahip bir kurulum uygulamasıdır. 
Aşağıda örnek bir kurulum adım adım anlatılmışır. 
Kurulum uygulaması resimde görülen adımların uygulanmasıyla gerçekleşir. 
Bütün adımlar için gerekli ayarlar yapıldıktan sonra **Yükle** adımı ile kurulum başlatılır.

<img src="/image/kurulum/0menu.png" alt="drawing" /> 

1- **Klavye**

Klavye seçimi yapılır. 
Masaüstü kurulumu için klavye yerleşim(Layout), minimal kurulumda Keymap değerleri çıkacaktır.

<img src="/image/kurulum/1klavye.png" alt="drawing"/>

2- **Bilgisayar Adı**

Bilgisayar adı girilir.

<img src="/image/kurulum/2bilgisayar.png" alt="drawing"/>

3- **Bölgesel Ayarlar**

İlgili bölgesel ayarlar seçilir. Sistemin dilini belirleyecektir.

<img src="/image/kurulum/3bolgesel.png" alt="drawing"/>
   
4- **Zaman Dilimi**

İlgili zaman dilimi seçilir, Türkiye için en alttaki Turkey değeri seçilecektir.

<img src="/image/kurulum/4zaman.png" alt="drawing"/>

5- **Root Hesabı**

Root hesabı için parola ataması yapılır.

<img src="/image/kurulum/5root.png" alt="drawing"/>

   
6- **Kullanıcı Hesabı**

Kullanıcı hesabı ayarları yapılır.
Kullanıcı giriş adı sisteme girerken kullanacağınız isimdir.
**Türkçe karakter kullanılamaz**.

<img src="/image/kurulum/6kull1.png" alt="drawing"/>

Kullanıcı gerçek adı, uzun ismi veya ad-soyad olarak düşünülebilir.
Türkçe karakter kullanılabilir.

<img src="/image/kurulum/6kull2.png" alt="drawing"/>
Kullanıcı için de bir parola ataması yapılır.

<img src="/image/kurulum/6kull3.png" alt="drawing"/>

Kullanıcı için eklencek grup varsa seçilir.
Varsayılan olarak kullanıcı için gerekli gruplarla kurulacaktır.
Tamam ile geçilir.

<img src="/image/kurulum/6kull4.png" alt="drawing"/>
   
7- **Önyükleyici** (Bootloader)

Önyükleyicinin kurulacağı disk seçilir. 

<img src="/image/kurulum/7onyukleyici.png" alt="drawing"/>

Varsayılan olarak evet seçilir.

<img src="/image/kurulum/7onyukleyici2.png" alt="drawing"/>

8- **Disk Bölümleme**

Sistemin kurulacağı disk bölümünün hazırlandığı adımdır.
Sistem hangi diske kurulacaksa o disk için cfdisk uygulaması çalıştırılır.
Cfdisk kullanımı için uygulamanın kullanım belgesinden yararlanılabilir.
Örnek kurulumda sdb seçilmiştir.

<img src="/image/kurulum/8bolumleme1.png" alt="drawing"/>

Sistemin kurulumu için en az bir ext4 biçiminde diske ihtiyaç vardır.
Ayrıca EFI kurulumu da yapılacaksa en az 100MB lık FAT32 biçiminde bir disk bölümü daha ayarlanmalıdır.

<img src="/image/kurulum/8bolumleme2.png" alt="drawing"/>
   
<img src="/image/kurulum/8bolumleme3.png" alt="drawing"/>

9- **Dosya Sistemi**
 
Sistemin kurulacağı disk bölümünün atanması adımıdır.
Örnek kurulumda sdb1 disk bölümüne kurulum yapılacaktır.
Varsayılan olarak **/** altına bağlanılmalıdır.
EFI bölümünüz yoksa oluşturunuz varsa sadece bağlama yapınız.
EFI bölümü için **/boot/efi** altına bağlama yapılmalıdır.
Sonrasında biçimlendirmesine onay verilip çıkış(bitti) yapılabilir.

<img src="/image/kurulum/9dosyasistemi1.png" alt="drawing"/>
   
<img src="/image/kurulum/9dosyasistemi2.png" alt="drawing"/>

<img src="/image/kurulum/9dosyasistemi3.png" alt="drawing"/>

<img src="/image/kurulum/9dosyasistemi4.png" alt="drawing"/>
   
10- **Sistemin Yüklenmesi**

Kuruluma geçmeden önce adımlarda yapılan ayarları kontrol edilebilir.

<img src="/image/kurulum/10ayarlar.png" alt="drawing"/>

Ayarlar kontrol edildikten sonra yükle adımı seçilerek sistemin yüklenmesi başlatılır.

<img src="/image/kurulum/10yukle1.png" alt="drawing"/>
<img src="/image/kurulum/10yukle2.png" alt="drawing"/>
<img src="/image/kurulum/10yukle3.png" alt="drawing"/>
<img src="/image/kurulum/10yukle4.png" alt="drawing"/>

Başarılı yükleme mesajıyla kurulum uygulamasından çıkılabilir 
veya sistem yeniden başlatılabilir.

<img src="/image/kurulum/10yukle5.png" alt="drawing"/>


### C- Klasik Kurulum

Adım adım aşağıdaki yönergelerini uygulanır.

1- **Sabit sürücülerin hazırlanması**

**Bu aşamada diskinizdeki veriler silinecektir!** Kullanılan programlar hakkında yeterli
bilginiz yoksa devam etmeniz kesinlikle önerilmez! Bu aşamaya geçmeden önce lütfen
fdisk ve cfdisk programları ile ilgili [belgeleri](http://belgeler.org/howto/partition-howto-fdisk.html) inceleyin. 
Sistemi kuracağınız bölümleri hazırlamak için fdisk veya cfdisk komutlarını kullanbilirsiniz.

Bu belgede sistem bir tane kök dizini üzerine kurulacağından, sabit disk üzerinde sadece bir 
bölüm oluşturulacaktır. Bunu yapmak için,

Sistemin kurulacağı sürücü belirlenir:
```
	lsblk
```

fdisk (veya cfdisk) programı belirtilen sürücü ile başlatılır:
```

    fdisk /dev/sdx
    
```
'n' komutu ile sürücüde yeni bir bölüm oluşturulur:
```

    Komut (yardım için m): n
```
Bölümün tipi 'primary' veya 'extended' olarak belirlenir. Varsayılan
ayar ile devam etmek için parametresiz girdi verilir:
```

    Partition type
       p   primary (0 primary, 0 extended, 4 free)
       e   extended (container for logical partitions)
    Select (default p): 
    Using default response p.
```
Disk bölüm numarası belirlenir, bu durumda da varsayılan ile ilerlemek yeterli
olacaktır:
```

    Disk bölümü numarası (1-4, default 1):
```
Bölümün başlangıç sektörü belirlenir, bu aşamada varsayılan kullanılabilecek ilk sektörden
başlar. Varsayılan ile devam edilir:

```
    First sector (2048-15730687, default 2048): 
```

Bölümün bitiş sektörü belirlenir. Burada direkt olarak numara kullanmak yerine başlangıç
sektörüne belirli bir alan büyüklüğünün eklenmesi ile hesaplamak genel olarak daha 
uygundur. Sadece kullanmak istediğiniz büyüklüğü belirtilen şekilde girin, örneğin 20 GB'lık
bir alan kullanacaksanız parametreniz +20G olacaktır. Varsayılan olarak girilmesi halinde 
program kullanılabilir tüm alanı ayıracaktır:

```
    Last sector, +sectors or +size{K,M,G,T,P} (2048-15730687, default 15730687): 
```

'w' komutu ile sürücüdeki değişiklikler kaydedilir:
```
    Komut (yardım için m): w
    The partition table has been altered.
    Calling ioctl() to re-read partition table.
    Syncing disks.
```

2- **Dosya sisteminin oluşturulması**

Formatlan bölümün kullanılabilmesi için bölümde linux dosya sistemi oluşturulur:
```
    mkfs.ext4 /dev/sdax
```

3- **Dosya sisteminin bağlanması**

Kuruluma hazırlanan bölüm host sisteme bağlanır:
```
    mount /dev/sdax /mnt
```

4- **Sistemin Kopyalanması** 

Host sistemdeki mevcut dosya sistemi hedefe kopyalanır:
```
    cp -axvu / /mnt
```

5- **initramfs (başlatıcı) oluşturulur:**
```
    mount --bind /dev /mnt/dev
    mount --bind /sys /mnt/sys
    mount --bind /proc /mnt/proc
    chroot /mnt dracut -N --force --xz --omit systemd /boot/initrd
```

6- **Gerekli grub ayarlarını yapılır:**
```
    grub-install --force --boot-directory=/mnt/boot /dev/sdy
    chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
```

<!-- title: Milis Linux 2.0 Ekim Sürümü (Beta) -->

<!--2020/10/29-->
27-10-2020 tarihli yeni çalışan ve kurulan imaj dosyamız oluşturuldu. 
Yeni beta sürümde yapılan değişiklikler aşağıda belirtilmiştir: 


- Linux çekirdeği 5.8.15 sürümüne güncellendi.
- Linux-firmware 20201022 sürümüne güncellendi.
- LLVM altyapı paketleri 10.0.1 sürümüne güncellendi.
- QT grafik paketleri 5.15.1 sürümüne güncellendi.
- Vlc uygulaması 3.0.11.1 sürümüne güncellendi.
- Vlc uygulamasının root hesabı ile açılabilmesi etkinleştirildi.
- MPS arayüz ve servis yönetim arayüz uygulamaları canlı imaja eklendi.
- Uygulama ve menü Türkçeleştirme çalışmaları devam etmektedir.
- Paket deposuna yeni paketler eklendi.

İndirme adresleri:

[XFCE4 Masaüstü](https://mls.akdeniz.edu.tr/iso/milis-2.0-xfce4-B10.27.iso)

[Minimal Sistem (Konsol)](https://mls.akdeniz.edu.tr/iso/milis-2.0-min-B10.26.iso)

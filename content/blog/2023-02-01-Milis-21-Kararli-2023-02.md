<!-- title: Milis Linux 2.1 Kararlı Şubat Sürümü Duyurusu -->

<!--2023/02/02-->
Dönemlik çalışmalarımız ardından 02-02-2023 tarihli yeni çalışan ve kurulan imaj dosyamız hazırlanmıştır. 
Bu sürümde temel sistem aynı kalmak üzere genel olarak masaüstü ve ilişkili uygulamalar üzerinde çalışma yapılmıştır.
Bu güncellemelerle beraber masaüstü yapımız soyutlanarak farklı pencere yöneticilerini destekleyen yeni bir ayar yapısı geliştirilmiştir.

Ayrıca bu sürüm için yapılan çalışmalar aşağıda ayrıntılı olarak listelenmiştir:

- Farklı pencere yöneticilerini destekleyen yeni masaüstü yapılandırmasına geçildi.
- Bu yapılandırma ile masaüstü açılışını kontrol eden dinit uygulaması geliştirildi.  
- Yeni masaüstü yapılandırmasında Wayfire(Tiling) ve Labwc(Stacking) pencere yöneticileri kullanıldı.
- Ly giriş yöneticisi yerine Greetd arkauç uygulamasını kullanan yeni masaüstü giriş yöneticisi arayüzü geliştirildi. 
- Yeni giriş yöneticisi ile masaüstüne girerken farklı pencere yöneticisi ve dil seçme seçeneği sunuldu.
- Ayar Merkezi(Ayguci) uygulamasına *Uygulama Atama* ve kullanıcı tanımlı *Klavye* ayar modülü eklendi.
- Masaüstü paneline hızlı ekran kayıt edici, ekran alıntılama ve hava durumu eklentileri eklendi.
- Fterm uygulamasına alternatif işlevsel dosya yöneticisi olarak kullanabilme özellikleri eklendi.
- PDF belgelerinin okunması için MPDF uygulaması geliştirilerek uygulama menüsüne eklendi.
- Yazıcı yönetim arayüz uygulaması, Milis servis destekli system-config-printer uygulaması olarak ayarlandı.
- Panelde MEG ekran alıntılama uygulamamıza işlevsel destek sunması için Swappy uygulaması da kullanıldı.
- Farklı pencere yöneticilerini destekleyecek şekilde ekran yönetim yapılandırmamız güncellenerek Ayar Merkezine eklendi. 
- Wayland-logout uygulaması yerine kendi masaüstü çıkış betiğimiz yazıldı.
- Komut satırına; Go, Rust ve Zig programlama dillerinin hızlı kurulum betiklerini eklendi.
- Uçbirim uygulamasına farklı yollarla çoklu sekme açma özelliği eklendi.
- Geany metin düzenleyicisine terminal desteği eklenerek 1.38 sürümüne güncellendi.
- Firefox tarayıcısı 105.0.1 sürümüne güncellendi.
- QT grafik paketleri 5.15.6 sürümüne güncellendi.
- Celluloid video oynatıcısına ek olarak VLC uygulaması da depoya eklendi.
- Htop süreç yöneticisi 3.2.1 sürümüne güncellenerek menü yerine konsoldan erişilecek şekilde ayarlandı.
- Masaüstü uygulama ve menüleri Türkçe ve İngilizce destekleyecek şekilde yeniden ayarlandı.
- Linux çekirdeği olarak halen 5.16.14 sürümü kullanılmakta olup ilerleyen sürümlerde 6.x sürümüne geçiş planlanmaktadır.
- Masaüstünde kullanılan varsayılan uygulama listesine [buradan](https://mls.akdeniz.edu.tr/git/milislinux/milis21/src/branch/master/belge/masaustu.md) erişilebilir.


İndirme adresi:

[Milis Linux 2.1 2023-02-02 Masaüstü](https://mls.akdeniz.edu.tr/iso/2.1/milis-2.1-desktop-2023.02.02.iso)

[İmaj Doğrulama](https://mls.akdeniz.edu.tr/iso/2.1/milis-2.1-desktop-2023.02.02.iso.sha256sum.txt)


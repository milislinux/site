<!-- title: Cumhuriyetimizin 100.Yılı ve Milis Linux -->

<!--2023/10/29-->
Tarihte bağımsızlığı için ağır bedeller ödemiş milletler vardır.
Bu milletlerin başında, bundan yüz yıl önce yedi düvele karşı savaşmış ve 
29 Ekim 1923'te Türkiye Cumhuriyeti Devleti olarak tarih sahnesinde yeniden doğmuş olan milletimiz yer almaktadır.
Bugün itibariyle Türkiye Cumhuriyetimizin 100.yılını kutlamaktayız.
Bugünlere gelebilmek milletimiz için kolay olmamıştır.
Kurtuluş Savaşı ile verilen kanlı mücadele sonunda kurulan devletimiz, 
sonrasında yapılan devrimler ile her alanda "muasır medeniyetler seviyesine erişme" idealini gerçekleştirme yoluna düşmüştür.

Gelişen dünyada meydana gelen değişiklikler, 2.Dünya Savaşı'ndan sonra elektronik bilgi çağının doğmasına neden olmuştur.
Bu çağda saha ve silah kavramları farklı olup artık savaşlar elektronik alanda verilmeye başlanmıştır.
Ülkemiz de bu alanda gerekli bilgi birikimini elde ederek başarılar elde etmeye çalışmıştır.
Fakat bu durum gelişen teknolojiyle beraber her on yılda daha rekabetçi ve zorlu bir hale gelmiştir.
Artık milletin her bir ferdi için gerekli olan kalem/kağıt okuryazarlığı yerini bilgisayar okuryazarlığına bırakmıştır.
Bilgisayar okuryazarlığı, bilgisayar donanımının ve yazılımının en iyi şekilde kullanılmasıdır.
Bir bilgisayarda temel yazılım, işletim sistemi olup bilişim alanında yapılacak olan faaliyetlerin taban sahasını oluşturmaktadır.
Yüzyıl önce savaş alanlarında verilen mücadele artık işletim sistemi üzerinden yapılan eylemlere dönüşmüştür.
Türkiye'de de bilgisayarın kullanımından beri yapılan uygulama geliştirmeleri ülkemizin bilişim alanındaki "hattı müdafaası" gibidir.
Fakat geliştirilen uygulamaların kısır ve kısıtlayıcı nitelikte olması bizim "sathı müdaafa" yapmamıza engel olmaktadır
Dolayısıyla bilişim alanında da bağımsız olabilmemiz için açık kaynaklar doğrultusunda geliştirilecek bir işletim sistemi büyük önem arzetmektedir.
Bu doğrultuda yüz yıl önceki Kuvayı Milliye hareketinden ilham alınarak yedi sene önce Milis Linux işletim sistemi çalışmaları gönüllü olarak başlatılmıştır. 
Milis Linux, Milis kelimesiyle "Milli, İşletim, Sistemi" kelimelerinin kısaltmasını oluştururken ayrıca gönüllü yurttaşların verdiği milli mücadeleye atfen Milis adını kullanmıştır.
2016'dan beri sırasıyla çıkartılan 1.0, 2.0, 2.1 sürümleri ardından Cumhuriyetimizin 100.yılına ithafen 2.3 sürüm çalışmamız hazır hale getirilmiştir.
On aylık bir uğraşın sonucu ilk deneme imajımız hazırlanarak yeni güncellemeler içerecek şekilde kullanıma sunulmuştur.

Aşağıda 2.3 sürümü ile yapılan değişikliklerin ayrıntıları yer almaktadır;

- Önceki sürümler saf 64 bit iken bu sürüm 32 bit destekli(multilib) olarak hazırlanmıştır.
- Grafik sunucu olarak Wayland kullanılmakta olup Wayfire ve Labwc pencere yöneticileri kullanılmıştır.
- Paketleme sürecinde yeni bir yaklaşıma gidilerek "bileşik paketleme" yöntemi geliştirilmiştir.
- Paket deposunda merkezi depo kullanımının yanında IPFS destekli dağıtık paket deposu desteği sağlanmıştır.
- MPS ayarları Lua yerine INI biçemine taşınarak daha kolay depo ekleme/güncelleme yapılması sağlanmıştır.
- MPS derleme sistemi uygulaması Go diliyle yeniden yazılarak daha basit ayar yapısına güncellenmiştir.
- Servis sistemi güncellenerek yönetimi tek bir betik üzerinden INI biçiminde ayar dosya yapısına aktarılmıştır.
- Tema seçenekleri açık ve koyu şekilde yer alarak kullanıcının tercihine sunulmuştur.
- Medya paketleri bileşik paketleme yaklaşımıyla paketlenerek daha geniş medya kütüphane desteği sağlanmıştır.
- Varsayılan dosya yöneticisi olarak Thunar kullanımına geçilmiştir.
- Daha kolay ve yönetilebilir bir bildirim yöneticisi kullanılmıştır.
- Açılış yükleyicisi (bootloader) olarak Grub yanında Limine desteği de eklenmiştir.
- Takvim yöneticisi olarak kullanıcı dostu Osmo uygulaması tercih edilmiştir.
- Ses yöneticisi olarak grafik arayüze sahip Pavucontrol uygulaması tercih edilmiştir.
- Çoğu paketin güncel sürümleri kullanılarak Türkçe desteği eklenerek depoya eklenmiştir.

Ve birçok hata düzeltme, güncelleme ve geliştirme çalışmaları katkıcı ekibimiz tarafından yapılarak bu sürümün hazırlanmasında rol oynamıştır. 
Aşağıdaki bağlantıdan deneme sürüm imajını indirerek yeni sistemi deneyimleyebilirsiniz.
Ayrıca Milis Linux'a katkıda bulunmak için geri dönüşlerinizi [iletişim kanallarını](https://mls.akdeniz.edu.tr/iletisim) kullanarak iletebilirsiniz.


İndirme adresi:

[Milis Linux 2.3 İmajı](https://mls.akdeniz.edu.tr/iso/2.3/beta/)

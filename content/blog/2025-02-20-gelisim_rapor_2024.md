<!-- title: Milis Linux Gelişim Raporu 2024 Yılı -->

Bu rapor, Milis Linux işletim sisteminin 2024 yılındaki gelişimini ve önemli kilometre taşlarını özetlemek amacıyla hazırlanmıştır.
Bu yıl yaptığımız çalışmalarla kararlılık, performans, güvenlik, kullanıcı deneyimi ve geliştirici araçları alanlarında önemli iyileştirmeler kaydedilmiştir. 
Aşağıda yapılan çalışmalara dönük ayrıntılar yer almaktadır.

**1. Masaüstü Ortamı İyileştirmeleri:**

-   **Pencere Yöneticisi:** Kullanıcılara masaüstü ortamı için daha fazla seçenek sunmak amacıyla [Hyprland](https://hyprland.org) ve [Louvre](https://github.com/CuarzoSoftware/Louvre) pencere yöneticileri paket deposuna eklenmiştir. Ayrıca Louvre geliştirme süreci yakından takibe alınarak uygulama bileşenleri güncel olarak paketlenmektedir.
-   **XWayland Desteği:** Labwc pencere yöneticisine Xwayland desteği eklenerek X tabanlı grafik uygulamaları için uyumluluk artırılmıştır.
-   **Güç Yönetimi:** Masaüstü ortamının [güç tüketimi](https://gitlab.com/milislinux/milis23/-/blob/main/belge/guc_yonetimi.md) için yeni bir ayar sistemi tasarlanıp masaüstü ayarlarına eklenmiştir. Bu çalışmayla enerji tasarrufu ve pil ömrü iyileştirmeleri için kullanıcı dostu konfigrasyon altyapısının kurulması hedeflenmiştir.

**2. Kullanıcı Deneyimi ve Yerelleştirme:**

-   **Giriş Yöneticisi:** Konsol tabanlı giriş yöneticisi [milis-greeter](https://gitlab.com/milislinux/milis-greeter) yerine grafik arayüzlü [mlogin](https://gitlab.com/milislinux/milis23/-/blob/main/bin/mlogin.py) uygulaması geliştirilerek varsayılan giriş yöneticisi olarak kullanılmaya başlanmıştır.
-   **Yerel Dil Desteği:** Yerel dil desteklerini kolaylaştırmak için gerekli betik geliştirilerek ayar merkezine yerelleştirme seçeneği eklenmiştir.

**3. Donanım Desteği ve Çekirdek Güncellemeleri:**

-   **Çekirdek Güncellemeleri:** Sistem çekirdeği, güvenlik ve performans iyileştirmeleri içeren 6.7.8 ve 6.11.5 sürümlerine güncellenmiştir.
-   **Nvidia Sürücüleri:** Nvidia ekran kartları için sürücü kurulumu iyileştirilmiş ve servis betikleri eklenerek sorunsuz bir kurulum deneyimi sağlanmıştır.

**4. Yazılım ve Kütüphane Güncellemeleri:**

-   **GTK4 Güncellemesi:** Grafik arayüzü geliştirmek için kullanılan GTK4 kütüphanesi ve bağımlı uygulamalar güncellenmiştir.
-   **Java SDK:** Java ve kütüphaneleri için [sdkman](https://sdkman.io) kurulum betiği eklenerek Java geliştirme ortamının kurulması kolaylaştırılmıştır.
-   **Yeni Programlama Dilleri:** Ada, [Zig](https://ziglang.org), [Vlang](https://vlang.io), R, PHP ve [Gleam](https://gleam.run) gibi yeni programlama dilleri için kurulum betikleri ve paketler depoya eklenerek farklı programlama dilleri desteği artırılmıştır.
-   **QT6 Kütüphanesi:** QT6 grafik kütüphanesi set olarak depoya eklenerek güncel grafiksel uygulamaların geliştirilmesi desteklenmiştir.
-   **Tarayıcı Güncellemeleri:** Firefox ve Chromium internet tarayıcıları güncellenerek kullanıcılara daha güvenli ve güncel bir internet deneyimi sunulmuştur.
-   **Ekran Kayıt Araçları:** Yeni ekran kayıt araçları (OBS-Studio, wl-screenrec) eklenerek kullanıcılara ekran kaydı için ek seçenekler sunulmuştur.

**5. Belgeleme ve Erişilebilirlik:**

-   **Belge Güncellemeleri:** Veritabanları ve programlama konularında belgeler eklenerek kullanıcılara daha fazla bilgi kaynağı sağlanmıştır.
-   **Erişilebilirlik Araçları:** Masaüstüne uzaktan erişim için [WayVNC](https://github.com/any1/wayvnc) ve [Guacamole](https://guacamole.apache.org) gibi araçlar eklenerek kullanıcılara farklı uzaktan erişim seçenekleri sunulmuştur.

**6. Yeni Projeler ve Araçlar:**

-   **MPS 3.0 Güncellemesi:** Paket yöneticisi [MPS](https://gitlab.com/milislinux/mps3), Go programlama dili ile yeniden geliştirilerek kayda değer performans iyileştirmeleri yapılmıştır. MPS'nin ayar yapısı INI formatına çevrilerek farklı geliştirme ortamlarına uyumu sağlanmıştır. Varsayılan olarak kullanılmak üzere 2.3 sürümünden 3.0 sürümüne geçiş tamamlanmıştır.
-   **Gitlab Entegrasyonu:** [Paket sunucu](https://gitlab.com/milislinux/depo) ve [kod yönetim](https://gitlab.com/milislinux) platformu olarak Gitlab kullanılmaya başlanmıştır.
-   **Ayguci Bilgi Sistemi:** [Ayguci](https://gitlab.com/milislinux/ayguci) bilgi sistemi güncellenerek ayar merkezi ile tam entegre hale getirilmiş ve bildirim modülü eklenmiştir.
-   **Deneysel Panel Çalışmaları:** [MPanel](https://gitlab.com/milislinux/mpanel) ve [YPanel](https://gitlab.com/sonakinci41/ypanel) adlı deneysel panel uygulamaları geliştirilerek kullanıcılara farklı panel seçenekleri sunulmuştur.

Sonuç olarak 2024 yılı da, Milis Linux işletim sistemi projesi için yoğun çalışmaların yapıldığı bir yıl olmuştur.
Kullanıcı deneyimi, performans, güvenlik ve geliştirici araçları alanlarında yapılan iyileştirmelerle dağıtımımız daha güçlü ve kullanıcı dostu bir hale getirilmeye çalışılmıştır. 
Gelecek yıllarda da kullanıcıların ihtiyaçlarına yönelik çalışmalarımıza devam edeceğiz.

<!--- 2025-02-20 -->
